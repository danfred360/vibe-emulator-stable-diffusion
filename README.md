# vibe emulator - stable diffusion
## Example usage
```powershell
(venv) PS C:\Users\danny\Documents\code\vibe-emulator-stable-diffusion> python project -f generate_image
ftfy or spacy is not installed using BERT BasicTokenizer instead of ftfy.
Enter desired prompt --> A photorealistic portrait of Satashi Nakamoto developing Bitcoin

        Desired prompt: A photorealistic portrait of Satashi Nakamoto developing Bitcoin
        Confirm? (y/n) --> y
50it [00:13,  3.57it/s]
Saved image with name A-photorealistic-portrait-of-09-02-2022_12-47-16.png in outputs directory.
Exiting...
```

Output:
![](outputs/A-photorealistic-portrait-of-09-02-2022_12-47-16.png)

An ad for my car:
![](outputs/jetta-ad.png)

## Getting Started

Use huggingface diffuser's library to run Stable Diffusion
```bash
pip install --upgrade diffusers transformers scipy
```

Login with HG token
```bash
huggingface-cli login
```

Run the pipeline with the defaul PNDM scheduler:
```python
import torch
from torch import autocast
from diffusers import StableDiffusionPipeline

model_id = "CompVis/stable-diffusion-v1-3"
device = "cuda"


pipe = StableDiffusionPipeline.from_pretrained(model_id, use_auth_token=True)
pipe = pipe.to(device)

prompt = "a photo of an astronaut riding a horse on mars"
with autocast("cuda"):
    image = pipe(prompt, guidance_scale=7.5)["sample"][0]  
    
image.save("astronaut_rides_horse.png")
```

> Note: If you are limited by GPU memory and have less than 10GB of GPU RAM available, please make sure to load the StableDiffusionPipeline in float16 precision instead of the default float32 precision as done above. You can do so by telling diffusers to expect the weights to be in float16 precision:
```python
import torch

pipe = StableDiffusionPipeline.from_pretrained(model_id, torch_dtype=torch.float16, revision="fp16", use_auth_token=True)
pipe = pipe.to(device)

prompt = "a photo of an astronaut riding a horse on mars"
with autocast("cuda"):
    image = pipe(prompt, guidance_scale=7.5)["sample"][0]  
    
image.save("astronaut_rides_horse.png")
```

To sqap out the noise scheduler, pass it to `from_pretrained`:
```python
from diffusers import StableDiffusionPipeline, LMSDiscreteScheduler

model_id = "CompVis/stable-diffusion-v1-3"
# Use the K-LMS scheduler here instead
scheduler = LMSDiscreteScheduler(beta_start=0.00085, beta_end=0.012, beta_schedule="scaled_linear", num_train_timesteps=1000)
pipe = StableDiffusionPipeline.from_pretrained(model_id, scheduler=scheduler, use_auth_token=True)
pipe = pipe.to("cuda")

prompt = "a photo of an astronaut riding a horse on mars"
with autocast("cuda"):
    image = pipe(prompt, guidance_scale=7.5)["sample"][0]  
    
image.save("astronaut_rides_horse.png")
```
## Resources
- https://huggingface.co/CompVis/stable-diffusion-v1-3
- https://pytorch.org
- [Generate speech from text, clone voices from mp3 files](https://replicate.com/afiaka87/tortoise-tts)
    - https://github.com/afiaka87/tortoise-tts
- [Animating images with Latent Space Navigation](https://replicate.com/wyhsirius/lia)

> @InProceedings{Rombach_2022_CVPR,
    author    = {Rombach, Robin and Blattmann, Andreas and Lorenz, Dominik and Esser, Patrick and Ommer, Bj\"orn},
    title     = {High-Resolution Image Synthesis With Latent Diffusion Models},
    booktitle = {Proceedings of the IEEE/CVF Conference on Computer Vision and Pattern Recognition (CVPR)},
    month     = {June},
    year      = {2022},
    pages     = {10684-10695}
}