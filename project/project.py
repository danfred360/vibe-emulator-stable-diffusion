import torch
import datetime
from torch import autocast
from diffusers import StableDiffusionPipeline, LMSDiscreteScheduler

def generate_image():
    model_id = "CompVis/stable-diffusion-v1-3"
    device = "cuda"

    scheduler = LMSDiscreteScheduler(beta_start=0.00085, beta_end=0.012, beta_schedule="scaled_linear", num_train_timesteps=1000)
    pipe = StableDiffusionPipeline.from_pretrained(model_id, torch_dtype=torch.float16, revision="fp16", scheduler=scheduler, use_auth_token=True)
    pipe = pipe.to(device)

    prompt = get_prompt()
    with autocast("cuda"):
        image = pipe(prompt, guidance_scale=7.5)["sample"][0]  
        
    sp = prompt.split(" ")[0:4]
    image_name = f'{sp[0]}-{sp[1]}-{sp[2]}-{sp[3]}-{datetime.datetime.now().strftime("%m-%d-%Y_%H-%M-%S")}.png'
    image.save(f'outputs/{image_name}')
    print(f'Saved image with name {image_name} in outputs directory.')
    print('Exiting...')

def get_prompt():
    invalid_prompt = True
    while invalid_prompt:
        query = input("Enter desired prompt --> ")
        invalid_prompt = validate_input("Desired prompt: {}".format(query))
    return query + " \n\n###\n\n"

def validate_input(query):
    user_validation = input("\n\t{}\n\tConfirm? (y/n) --> ".format(query))
    match user_validation:
        case "y":
            return False
        case "n":
            return True
        case _:
            print("\n\tInvalid response for user query validation.\n")
            validate_input(query)